#!/bin/sh
if ps -ef | grep -v grep | grep consumer.py ; then
    exit 0
else
    /opt/consumer.py >> ~/consumer.log &
    exit 0
fi