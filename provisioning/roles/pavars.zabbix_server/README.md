Zabbix Server
=========

Install Zabbix Server 5.0.x with Nginx and MariaDB backend. Default configuration files provided in templates

Role Variables
--------------

```yaml
zabbix_db_name: string (zabbix)
zabbix_db_user: string (zabbix)
zabbix_db_password: string (Zabbix1!)
mysql_root_password: string (Qwerty1!)
php_timezone: string (Europe/Riga)
```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: pavars.zabbix_server, php_timezone: "Europe/Riga" }
