# Twino homework

Set up environment on a Vagrant boxes with AMQP broker, message publisher, message consumer and a monitoring system. Monitor queue length with alerting enabled when length exceeds certain threshold.

## Pre-requisites

* Ansible 2.9.x
* Vagrant 2.2.x
* Virtualbox 6.x
* Python3 with pip3

# General setup

**central** box is set up with:

* Ubuntu 18.04 (hashicorp/bionic64)
* IP: 10.100.10.254
* RabbitMQ
* MariaDB
* Python 3
* Zabbix Server 5
* Zabbix Web nginx 5
* Zabbix Agent 5
* publisher.py script
* stress-test.sh script

RabbitMQ management is available on: 10.100.10.254:15672
```yaml
user: twino
password: twino
```

Zabbix is available on: 10.100.10.254
```yaml
user: Admin
password: zabbix
```

**worker** boxes are set up with:

* Ubuntu 18.04 (hashicorp/bionic64)
* Python 3
* Zabbix Agent 5
* consumer.py script
* consumer.sh script
* crontab entry to start consumer.sh under vagrant

## Environment variables

Add to ~/.zshrc or ~/.bashrc

```bash
# add VirtualBox to Path, location is OS specific
export PATH="$PATH:/mnt/c/Program Files/Oracle/VirtualBox"
PATH=$HOME/.local/bin:$PATH
VAGRANT_EXPERIMENTAL=disks
# Only applicable to WSL hosts to enable passthrough to Windows
export VAGRANT_WSL_ENABLE_WINDOWS_ACCESS="1"
```

# Basic usage

A short summary on how to use this project

## First run this

With default configuration it starts 5 workers and 1 central Vagrant box in Virtualbox, it can be changed in Vagrantfile

```bash
# Clone this repository
git clone https://gitlab.com/pavars/twino-vagrant

# Start Vagrant from project source
vagrant up
```
### Post-install

**After central box is up and Zabbix server is provisioned import template for RabbitMQ from scripts/zbx_export_templates.xml**

This can be done via Frontend Configuration -> Templates -> [Import](http://10.100.10.254/conf.import.php), leave checkboxes to default

This adds 1 trigger to default template which raises a problem if there are more than 100 messages in the queue. Trigger can be changed in Templates -> Template App RabbitMQ cluster by Zabbix agent -> Macros -> {$MQ_TOTAL_WARN} or on host level


### To start publishing messages:

```bash
vagrant ssh central
/opt/publisher.py n # where n is time in seconds for worker to sleep
```

### Log and status monitoring:

```bash
# Check worker status
vagrant ssh worker-n
tail -f /home/vagrant/consumer.log

# Check publisher status
vagrant ssh central
tail -f /home/vagrant/publisher.log

# Check RabbitMQ queue status
sudo rabbitmqctl list_queues
```

### To start stress test:

```bash
vagrant ssh central
/opt/stress_test.sh n # Where n is how many messages to queue with random sleep values between 10 and 100
```

### Maintenance

```bash
# Remove all VMs
vagrant destroy

# Re-provision all VMs
vagrant provision

# Start new VMs and force reprovision
vagrant up --provision
```

## Configuration options

Most required variables for development purposes are added in Ansible roles and they can be specified in Vagrantfile under *configuration parameters*

```rb
#defaults
CENTRAL_IP = "10.100.10.254"
RABBITMQ_USER = "twino"
RABBITMQ_PASSWORD = "twino"
PYTHON_INTERPRETER = "/usr/bin/python3"
MYSQL_ROOT_PASSWORD = "Qwerty1!"
```

They can be specified in Ansible roles/ playbooks specifically if needed. Some defaults are provided in each role

```yaml
rabbitmq_user: "twino"
rabbitmq_password: "twino"
zabbix_server_ip: "10.100.10.254"
mysql_root_password: "Qwerty1!"
php_timezone: "Europe/Riga"
```

## Vagrantfile

To increase or decrase worker count, change the range in line **(1..n).each do |i|** where *n* is the number of workers to start- min 1; max 252

This Vagrant file includes boxes:
* central, IP: 10.100.10.2, CPUs: 2, RAM: 1024, Disk 64Gb
* worker-N (default 5), IP: dhcp, CPUs: 1, RAM: 1024, Disk 64Gb

They are added to private network 10.100.10.0/24, with central box fixed IP 10.100.10.2

```rb
### configuration parameters ###
CENTRAL_IP = "10.100.10.254"
RABBITMQ_USER = "twino"
RABBITMQ_PASSWORD = "twino"
PYTHON_INTERPRETER = "/usr/bin/python3"
MYSQL_ROOT_PASSWORD = "Qwerty1!"
### /configuration parameters ###

Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
  config.ssh.private_key_path = ['~/.vagrant.d/insecure_private_key',"~/.ssh/id_rsa"]
  config.ssh.insert_key = false
  config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "~/.ssh/authorized_keys"
  config.vm.define "central" do |central|
    central.vm.network "private_network", ip: CENTRAL_IP
    central.vm.hostname = "central"
    central.vm.provider "virtualbox" do |v|
      v.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
      v.name = "central"
      v.memory = 1024
      v.cpus = 2
    end
  end
  (1..5).each do |i|
    config.vm.define "worker-#{i}" do |worker|
      worker.vm.network "private_network", ip: "10.100.10.#{i+1}"
      worker.vm.hostname = "worker-#{i}"
      worker.vm.provider "virtualbox" do |v|
        v.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
        v.name = "worker_#{i}"
        v.memory = 1024
        v.cpus = 1
      end
    end
  end
  config.vm.provision "ansible" do |ansible|
      ansible.playbook = "provisioning/rabbitmq.yml"
      ansible.groups = {
        "master" => ["central"],
        "workers" => ["worker-[1:1000]"],
        "workers:vars" => { "ansible_python_interpreter" => PYTHON_INTERPRETER,
                      "rabbitmq_user" => RABBITMQ_USER,
                      "rabbitmq_password" => RABBITMQ_PASSWORD,
                      "zabbix_server_ip" => CENTRAL_IP}
      }
      ansible.host_vars = {
        "central" => { "ansible_python_interpreter" => PYTHON_INTERPRETER,
                      "rabbitmq_user" => RABBITMQ_USER,
                      "rabbitmq_password" => RABBITMQ_PASSWORD,
                      "mysql_root_password" => MYSQL_ROOT_PASSWORD,
                      "zabbix_server_ip" => CENTRAL_IP}
      }
    end
end

```

## Scripts

All scripts are placed in /opt/

* [publisher.py](provisioning/scripts/publisher.py) n

Takes 1 argument n(int) that pushes message to RabbitMQ with that payload. If no values are specified - default is 0 

* [stress_test.sh](provisioning/scripts/stress_test.sh) n

Takes 1 argument n(int) that invokes publisher.py n times with random payload values. If no values are specified - default is 1

* [consumer.py](provisioning/scripts/consumer.py)

This is a RabbitMQ worker script that gets messages from queue "sleep" with user "twino"

* [consumer.sh](provisioning/scripts/consumer.sh)

This is a simple script that starts consumer.py if it's not running

* [enable_autoregistration.py](provisioning/scripts/enable_autoregistration.py)

This script enables autoregistration for Zabbix


# Monitoring

Monitoring is done by Zabbix which is installed on central box http://10.100.10.254/ The configuration is managed by ansible and for development shouldn't be changed, during installation it adds 2 "Autoregistration actions" for Linux hosts and for RabbitMQ server which is done by ZabbixAPI (scripts/enable_autoregistration.py)

The agents are deployed using ansible and configured to use autoregistration which will add Templates to them: "Template OS Linux by Zabbix agent" and "Template App RabbitMQ cluster by Zabbix agent"

Status and problems can be monitored in Monitoring -> Problems | Monitoring -> Dashboard -> Global View | Monitoring -> Latest data

# Ansible roles

* [pavars.consumer](provisioning/roles/pavars.consumer) - installs Python worker
* [pavars.publisher](provisioning/roles/pavars.publisher) - installs Python publisher and stress-test
* [pavars.dependencies](provisioning/roles/pavars.dependencies) - installs Python dependencies
* [pavars.rabbitmq_server](provisioning/roles/pavars.rabbitmq_server) - installs RabbitMQ 
* [pavars.zabbix_agent](provisioning/roles/pavars.zabbix_agent) - installs Zabbix Agent
* [pavars.zabbix_server](provisioning/roles/pavars.zabbix_server) - installs MariaDB and Zabbix server

