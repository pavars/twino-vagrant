Dependencies
=========

Includes Python3 dependencies and some common software for Linux servers. This role also installs ntpd and sets timezone to Europe/Riga

* curl
* git
* software-properties-common
* wget
* unzip
* zip
* pika
* ntpd
* python3
* pip3

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - pavars.dependencies
