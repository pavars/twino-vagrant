#!/usr/bin/env python3
import pika
import time
import logging

rabbitmq_user=''
rabbitmq_password=''
rabbitmq_host=''
# create logger with 'consumer'
logger = logging.getLogger("consumer")
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('consumer.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

# authenticate with 
creds = pika.PlainCredentials(rabbitmq_user, rabbitmq_password)
try:
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=rabbitmq_host, credentials=creds))
except:
    logger.error("Connection can't be established")
    exit
    
channel = connection.channel()
channel.queue_declare(queue='sleep')

def callback(ch, method, properties, body):
    logger.info(" [x] Received %r" % body)
    time.sleep(int(body))
    logger.info(" [x] Done")
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_consume(
    queue='sleep', on_message_callback=callback)

logger.info(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()