Zabbix Agent
=========

Install Zabbix Agent 5.0

Variables
----------------

```yaml
zabbix_server_ip: string
```
Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: pavars.zabbix_agent, zabbix_server_ip: "10.100.10.254" }

