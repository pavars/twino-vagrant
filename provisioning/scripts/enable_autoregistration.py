#!/usr/bin/env python3
from pyzabbix.api import ZabbixAPI
from urllib.error import HTTPError

# Create ZabbixAPI class instance
try:
   zapi = ZabbixAPI(url='http://localhost/', user='Admin', password='zabbix')
except HTTPError:
   print("Check your connection or credentials")

try:
    linux_autoreg = zapi.do_request('action.create',
        {
        "name":"Linux host autoregistration",
        "eventsource":"2",
        "status":"0",
        "esc_period":"1h",
        "filter":{
           "evaltype":"0",
           "formula":"",
           "conditions":[
              {
                 "conditiontype":"24",
                 "operator":"2",
                 "value":"Linux",
                 "value2":"",
                 "formulaid":"A"
              }
           ]
        },
        "operations":[
           {
              "operationtype":"6",
              "esc_period":"0",
              "esc_step_from":"1",
              "esc_step_to":"1",
              "evaltype":"0",
              "opconditions":[
              ],
              "optemplate":[
                 {
                    "templateid":"10001"
                 }
              ]
           }
        ]
        })
except:
    print("Skipping Linux autoreg")

try:
    rabbitmq_autoreg = zapi.do_request('action.create',
        {
        "name":"RabbitMQ host autoregistration",
        "eventsource":"2",
        "status":"0",
        "esc_period":"1h",
        "filter":{
           "evaltype":"0",
           "formula":"",
           "conditions":[
               {
                  "conditiontype":"22",
                  "operator":"2",
                  "value":"Zabbix server",
                  "value2":"",
                  "formulaid":"A"
               }
           ]
        },
        "operations":[
            {
               "operationtype":"6",
               "esc_period":"0",
               "esc_step_from":"1",
               "esc_step_to":"1",
               "evaltype":"0",
               "opconditions":[

               ],
               "optemplate":[
                  {
                     "templateid":"10300"
                  }
               ]
            }
        ]
        })
except:
    print("Skipping RabbitMQ autoreg")

# Logout from Zabbix
zapi.user.logout()