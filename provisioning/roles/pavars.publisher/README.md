Python Publisher
=========

Publisher script written in Python3, uses pika for communication. Default logging is set to ~/publisher.log

All scripts are placed in /opt/

* publisher.py n
Takes 1 argument n(int) that pushes message to RabbitMQ with that payload. If no values are specified - default is 0 

* stress_test.sh n
Takes 1 argument n(int) that invokes publisher.py n times with random payload values. If no values are specified - default is 1

Dependencies
------------

Ansible role: pavars.dependencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - pavars.publisher