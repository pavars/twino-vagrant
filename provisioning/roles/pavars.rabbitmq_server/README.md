RabbitMQ server
=========

This role installs RabbitMQ from Bintray repositories with Erlang 22.x and marks it as preferred source. This sets up 2 users for RabbitMQ - *twino* with full admin access and *zbx_monitor* with monitoring access

Variables
----------------

```yaml
rabbitmq_user: string
rabbitmq_password: string
```

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: pavars.rabbitmq_server, rabbitmq_user: "twino", rabbitmq_password: "twino" }
