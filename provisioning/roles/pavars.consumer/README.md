Python Consumer
=========

Ansible role that installs Python consumer

Requirements
------------

* Python3
* Pip3
* pika
* pavars.dependencies installs these dependencies


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - pavars.dependencies
         - pavars.consumer