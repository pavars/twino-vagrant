#!/usr/bin/env python3
import pika
import sys
import logging

# create logger with 'publisher'
logger = logging.getLogger("publisher")
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('publisher.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

try:
    seconds = int(sys.argv[1])
except ValueError:
    logger.error("Please enter time in seconds n")
    seconds = 0
except IndexError:
    logger.error("No time specified")
    seconds = 0

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='sleep')

channel.basic_publish(exchange='',
                      routing_key='sleep',
                      body=str(seconds))
logger.info("[x] Sent Sleep {} seconds".format(seconds))

connection.close()